﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Sceys.Models;
using System.Data;

namespace Sceys.Controllers
{
    public class PersonasManagerController : Controller
    {
        // instancia del contexto, se importa el Models
        private SceysContext db = new SceysContext();

        // GET: PersonasManager
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarCentros(string RegionalId) {

            var regionalId = Convert.ToInt32(RegionalId);
            var centros = from Centros in db.Centros
                          where Centros.RegionalId == regionalId
                          select new {
                              centroId = Centros.CentroId,
                              nombreCentro = Centros.Nombre
                          };
            return Json(centros, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult consultarPrograma(string centroId) {
            return null;
        }
    }
}
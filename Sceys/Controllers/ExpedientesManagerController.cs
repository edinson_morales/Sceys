﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Sceys.Models;
using System.Data;

namespace Sceys.Controllers
{
    public class ExpedienteMainController : Controller
    {
        // instancia del contexto, se importa el Models
        private SceysContext db = new SceysContext();

        [HttpPost]
        // GET: ExpedienteMain
        public ActionResult Index(string Prefix)
        {
            //un informante(el que pone la queja) puede ser un aprendiz, instructor, o cualquier funcionario del sena.
            var Informante = db.Personas.Include(p => p.TipoPersona);
            return Json(Informante, JsonRequestBehavior.AllowGet);
        }

        //Consultar informantes, que pueden ser aprendices, instructores y funcionarios
        [HttpPost]
        public JsonResult consultaPersonas(string term)
        {
            var Informante = (from Personas in db.Personas
                              join TipoPersona in db.TipoPersonas on Personas.TipoPersonaId equals TipoPersona.TipoPersonaId
                              join TipoDocumento in db.TipoDocumentos on Personas.TipoDocumentoId equals TipoDocumento.TipoDocumentoId

                              where Personas.Documento.Contains(term) ||
                                     Personas.PrimerNombre.Contains(term.ToLower()) ||
                                     Personas.SegundoNombre.Contains(term.ToLower()) ||
                                     Personas.PrimerApellido.Contains(term.ToLower()) ||
                                     Personas.SegundoApellido.Contains(term.ToLower())
                              select new
                              //Información detallada de la persona, para mostrar en las búsquedas para cada vista. 
                              {
                                  nombre = Personas.PrimerNombre + " " + Personas.SegundoNombre + " " + Personas.PrimerApellido + " " + Personas.SegundoApellido,
                                  tipoDoc = TipoDocumento.Descripcion,
                                  documento = Personas.Documento,
                                  correo = Personas.Correo,
                                  celular = Personas.Celular,
                                  telefono = Personas.Telefono,
                                  direccion = Personas.Direccion,
                                  tipoPer = TipoPersona.Descripcion
                              }).Take(10);
            return Json(Informante, JsonRequestBehavior.AllowGet);
        }

        //consulta únicamente los aprendices, a quienes se los lleva a comité
        [HttpPost]
        public JsonResult consultaAprendiz(string term)
        {

            var Aprendices = from Personas in db.Personas
                             join TipoPersona in db.TipoPersonas on Personas.TipoPersonaId equals TipoPersona.TipoPersonaId
                             join TipoDocumento in db.TipoDocumentos on Personas.TipoDocumentoId equals TipoDocumento.TipoDocumentoId
                             join Aprendiz in db.Aprendizs on Personas.PersonaId equals Aprendiz.PersonaId

                             where (Personas.Documento.Contains(term) ||
                                    Personas.PrimerNombre.Contains(term.ToLower()) ||
                                    Personas.SegundoNombre.Contains(term.ToLower()) ||
                                    Personas.PrimerApellido.Contains(term.ToLower()) ||
                                    Personas.SegundoApellido.Contains(term.ToLower())) &&
                                    TipoPersona.Descripcion == "Aprendiz"
                             select new
                             //Información detallada de la persona, para mostrar en las búsquedas para cada vista. 
                             {
                                 nombre = Personas.PrimerNombre + " " + Personas.SegundoNombre + " " + Personas.PrimerApellido + " " + Personas.SegundoApellido,
                                 tipoDoc = TipoDocumento.Descripcion,
                                 documento = Personas.Documento,
                                 correo = Personas.Correo,
                                 celular = Personas.Celular,
                                 telefono = Personas.Telefono,
                                 direccion = Personas.Direccion,
                                 tipoPer = TipoPersona.Descripcion,
                                 ficha = Aprendiz.Ficha,                                 
                                 programa = (from programaxcentro in db.ProgramaCentros
                                        join Aprendix in db.Aprendizs on programaxcentro.ProgramaCentroId equals Aprendix.ProgramaCentroId
                                        join programa in db.Programas on programaxcentro.ProgramaId equals programa.ProgramaId
                                        where Aprendix.PersonaId == Personas.PersonaId
                                        select programa.Nombre
                                      ).FirstOrDefault()
                           };
            return Json(Aprendices, JsonRequestBehavior.AllowGet);
        }

        //Los que reciben los documentos son únicamente los funcionarios del centro
        [HttpPost]
        public JsonResult consultaReceptor(string term)
        {
            var Aprendiz = (from Personas in db.Personas
                            join TipoPersona in db.TipoPersonas on Personas.TipoPersonaId equals TipoPersona.TipoPersonaId
                            join TipoDocumento in db.TipoDocumentos on Personas.TipoDocumentoId equals TipoDocumento.TipoDocumentoId
                            //join Aprendiz in db.Aprendizs on Personas.PersonaId equals Aprendiz.PersonaId

                            where (Personas.Documento.Contains(term) ||
                                   Personas.PrimerNombre.Contains(term.ToLower()) ||
                                   Personas.SegundoNombre.Contains(term.ToLower()) ||
                                   Personas.PrimerApellido.Contains(term.ToLower()) ||
                                   Personas.SegundoApellido.Contains(term.ToLower())) &&
                                   TipoPersona.Descripcion != "Aprendiz" &&
                                   TipoPersona.Descripcion != "Instructor"
                            select new
                            //Información detallada de la persona, para mostrar en las búsquedas para cada vista. 
                            {
                                nombre = Personas.PrimerNombre + " " + Personas.SegundoNombre + " " + Personas.PrimerApellido + " " + Personas.SegundoApellido,
                                tipoDoc = TipoDocumento.Descripcion,
                                documento = Personas.Documento,
                                correo = Personas.Correo,
                                celular = Personas.Celular,
                                telefono = Personas.Telefono,
                                direccion = Personas.Direccion,
                                tipoPer = TipoPersona.Descripcion
                            }).Take(3);
            return Json(Aprendiz, JsonRequestBehavior.AllowGet);
        }

        //REGLAMENTO DEL APRENDIZ
        //Consultar los artículos para cada capítulo
        [HttpPost]
        public JsonResult consultaArticulo(string capitulo)
        {
            int capituloId = int.Parse(capitulo);
            var articulos = from Articulo in db.Articulos
                            join Capitulo in db.Capitulos on Articulo.CapituloId equals Capitulo.CapituloId
                            where Articulo.CapituloId == capituloId
                            select new
                            {
                                articuloId = Articulo.ArticuloId,
                                descripcion = Articulo.Descripcion,
                                descripcion2 = Articulo.Descripcion2
                            };
            return Json(articulos, JsonRequestBehavior.AllowGet);
        }

        //Consultar los numerales para cada artículo
        [HttpPost]
        public JsonResult consultarNumeral(string articulos)
        {
            var articuloId = int.Parse(articulos);
            var numerales = from Numeral in db.Numerals
                            join Articulos in db.Articulos on Numeral.ArticuloId equals Articulos.ArticuloId
                            where Numeral.ArticuloId == articuloId
                            select new
                            {
                                numeralId = Numeral.NumeralId,
                                titulo = Numeral.Titulo,
                                descripcion = Numeral.Descripcion
                            };
            return Json(numerales, JsonRequestBehavior.AllowGet);
        }

        //registrar Expedientes
        [HttpPost]
        public JsonResult registrarExpedientes(ExpedienteView expedienteView, List<DatosRecepcionDocumentos> datos)
        {
            //variables de datos
            var cont = 0;
            var fecha = DateTime.Now;
            var documentoReceptor = Request.Form["documentoReceptor"];           
                
                
                
            expedienteView.descripcionFalta = Request.Form["normas"];

            /*
            * Guardamos en múltiples tablas
            * creamos una transacción
            * se guardan todos los registros o no se guarda ninguno             
            */

            //PARTE 1. Se Crea el Expediente para cada aprendiz
            foreach (var item in datos)
            {                
                if (item.tipo == "Aprendiz")
                {
                    var aprendizId = (from Personas in db.Personas
                                      join Aprendices in db.Aprendizs on Personas.PersonaId equals Aprendices.PersonaId
                                      where Personas.Documento == item.documento
                                      select Aprendices.AprendizId
                                     ).FirstOrDefault();
                    var Expedientes = new Expediente
                    {
                        AprendizId = aprendizId,
                        Fecha = fecha,
                        Estado = 0,
                        descripcionQueja = "url folder Expediente"
                    };
                    db.Expedientes.Add(Expedientes);
                    db.SaveChanges();

                    //PARTE 2. A cada expediente se registra el receptor de la documentación y los informantes del expediente
                             
                    //Para cada aprendiz se registra quiénes fueron los informantes y el receptor
                    foreach (var item2 in datos)
                    {
                        if (item2.tipo == "Informante" || item2.tipo =="Receptor")
                        {
                            //Involucrados en el proceso, Informantes y demás
                            var personaId = (from Personas in db.Personas
                                             where Personas.Documento == item2.documento
                                             select Personas.PersonaId
                                             ).FirstOrDefault();
                            var RecepcionDocumentos = new RecepcionDocumento
                            {
                                ExpedienteId = Expedientes.ExpedienteId,
                                PersonaId = personaId, //receptor
                                Tipo = item2.tipo
                            };
                            db.RecepcionDocumentoes.Add(RecepcionDocumentos);
                            db.SaveChanges();                            
                        }
                    }                    
                    //Para cada aprendiz se registran las faltas en su expediente
                    foreach(var item3 in datos){                    

                        // Recolección de información
                        if (item3.tipo == "faltaId") {
                            expedienteView.tipoFaltaId = int.Parse(item3.documento);
                        }

                        if (item3.tipo == "calificacionId")
                        {
                            expedienteView.calificacionId = int.Parse(item3.documento);
                        }

                        if (item3.tipo == "normas") {
                            expedienteView.descripcionFalta = item3.documento;
                        }                        
                    }
                    //registro en db                       
                    var objFaltas = new Falta
                    {
                        ExpedienteId = Expedientes.ExpedienteId,
                        TipoFaltaId = expedienteView.tipoFaltaId,
                        CalificacionId = expedienteView.calificacionId,
                        DescripcionFaltas = expedienteView.descripcionFalta
                    };
                    db.Faltas.Add(objFaltas);
                    db.SaveChanges();
                }                                                              
            }
            return Json(cont, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult registrarPreExpediente() {


            return null;
        }
    }
}
@{
    ViewBag.Title = "Sceys";
}
<link href="~/Content/sceys/about.css" rel="stylesheet" />

<div class="header-index">
    <div class="sobre-header-index">
        <img src="~/Imagen/300x150.png" alt="">
    </div>
</div>
<div class="contendo-index">
    <div class="radio">
        <i class="glyphicon glyphicon-chevron-down down"></i>
    </div>
    <div style="background:#fff">
        <div class="container">
            <div class="row">
                <div class="centrar-texto">
                    <p>
                        El Sceys es un Software Desarrollado para nuestra instituci�n SENA, utilizado por el Comit� de Evaluaci�n y Seguimiento que es el encargado de llevar un control de la vida acad�mica del aprendiz en el desarrollo de su formaci�n en la etapa lectiva y pr�ctica.

                        Este nos permite gestionar y suministrar toda la informaci�n necesaria para generar un reporte de falta acad�mica y/o disciplinaria en los Centros de formaci�n seg�n lineamientos institucionales.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="titlo-contenedor">
        <h3>M�DULOS</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <a class="card-item" href="FaseFormativa.html">
                    <div class="col-md-5 col-xs-12 col-lg-5" style="padding-left:0">
                        <img src="~/Imagen/fase1.jpg" class="img-items" alt="">
                    </div>
                    <div class="col-md-7 col-xs-12 col-lg-7">
                        <div class="texto-items">
                            <span class="titulo-item-card">FASE FORMATIVA</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 col-xs-12 col-lg-12">
                <a class="card-item" href="FaseComite.html">
                    <div class="col-md-5 col-xs-12 col-lg-5" style="padding-left:0">
                        <img src="~/Imagen/fase2.jpg" class="img-items" alt="">
                    </div>
                    <div class="col-md-7 col-xs-12 col-lg-7">
                        <div class="texto-items">
                            <span class="titulo-item-card">FASE COMIT�</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 col-xs-12 col-lg-12" >
                <a class="card-item">
                    <div class="col-md-5 col-xs-12 col-lg-5" style="padding-left:0">
                        <img src="~/Imagen/fase3.jpg" class="img-items" alt="">
                    </div>
                    <div class="col-md-7 col-xs-12 col-lg-7">
                        <div class="texto-items">
                            <span class="titulo-item-card">FASE ACOMPA�AMIENTO</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="~/Scripts/sceys/about.js"></script>
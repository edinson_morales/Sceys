﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sceys.Startup))]
namespace Sceys
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

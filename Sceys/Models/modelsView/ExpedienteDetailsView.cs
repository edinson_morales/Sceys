﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models.modelsView
{
    public class ExpedienteDetailsView
    {
        /*
            *
            * Información detallada del expediente
            * 
        */

        // Información de la tabla Expedientes
        public int ExpedienteId { get; set; }
        public string documentoAprendiz { get; set; }
        public string nombreAprendiz { get; set; }
        public string correoAprendiz { get; set; }
        public string fichaAprendiz { get; set; }
        public string programaAprendiz { get; set; }        
        public string fecha { get; set; }
        public int Estado { get; set; }
        public string descripcionQueja { get; set;}

        // Información de la tabla Faltas y sus relaciones. 
        public string tipoFalta { get; set; }
        public string calificacionFalta { get; set; }                
        public string DescripcionFaltas { get; set; }        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class PersonaView
    {
        public Persona Persona { get; set; }

        public TipoDocumento TipoDocumento { get; set; }

        public TipoPersona TipoPersona { get; set; }

        public Aprendiz Aprendiz { get; set; }

        public Programa Programa { get; set; }

        public ProgramaCentro ProgramaCentro { get; set; }

        public Centro Centro { get; set; }

        public Regional Regional { get; set; }

        public TipoPrograma TipoPrograma { get; set; }
    }
}
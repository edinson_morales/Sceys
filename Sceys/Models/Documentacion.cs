﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Documentacion
    {

        [Key]
        public int DocumentacionId { get; set; }

        public int ExpedienteId { get; set; }

        public int TipoDocumentacionId { get; set; }

        public int PersonaId { get; set; }

        public string nombreArchivo { get; set; }

        public string comentarioArchivo { get; set; }

        public DateTime fecha { get; set; }

        //Tablas relacionadas
        public virtual Persona Persona { get; set; }

        public virtual TipoDocumentacion TipoDocumentacion { get; set; }

        public virtual Expediente Expediente { get; set; }


        /*
        public List<Documentacion> Adjuntos(int? ExpedienteId){
            var adjuntos = new List<Documentacion>();
            var expedienteId = Convert.Int32(ExpedienteId);
            try{
                adjuntos = db.Documentacion()
                             .Where(d=>d.ExpedienteId == expedienteId)
                           .ToList();

            }catch(Exception e){
                throw new Exception(e.Message);

            }
            return adjuntos;
  
            return null;
	}  */

    }
}

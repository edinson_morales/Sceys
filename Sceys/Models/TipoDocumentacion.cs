﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class TipoDocumentacion
    {
        [Key]
        public int TipoDocumentacionId { get; set; }

        public string Descripcion { get; set; }

        //Se relacionó con
        public ICollection<Documentacion> Documentacion { get; set; }

    }
}